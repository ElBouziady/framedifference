#-------------------------------------------------
#
# Project created by QtCreator 2017-07-12T11:34:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = frameDifference
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui


LIBS += `pkg-config --libs opencv`
LIBS+= -L"/usr/local/lib/"  -lopencv_highgui -lopencv_core -lopencv_imgcodecs
INCLUDEPATH += '/usr/local/include'
