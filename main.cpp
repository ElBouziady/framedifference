#include "mainwindow.h"
#include <QApplication>

#include <iostream>
#include <stdio.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <QDir>

using namespace cv;
using namespace std;

bool frameDifferencing( Mat img_input_prev, Mat img_input );

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    Mat frame_input,frame;
    QString dir_input="/media/boukary/LENOVO_USB_HDD/bouknadel/Prise_2/prise2/images/";
    QDir dir(dir_input);
    QStringList filtre;
    filtre<<"*.bmp";
    QFileInfoList images_path;
    images_path = dir.entryInfoList(filtre,QDir::Files);


    QString dir_outputBG="/media/boukary/LENOVO_USB_HDD/output_BGS/bg/"; // /home/boukary/Pictures/test/bg/
    QString dir_output_nonBG="/media/boukary/LENOVO_USB_HDD/output_BGS/non_bg/"; // /home/boukary/Pictures/test/not_bg/
    QString chemin;



    Mat prev,curr;

    for(int i=0;i<images_path.size();i+=10)
    {


        frame=imread(images_path.at(i).absoluteFilePath().toStdString());

        //cap>>frame;
        std::cout << "Press 'q' to quit..." << std::endl;
        cv::Mat curr(frame);

        cv::namedWindow("Input");
        cv::imshow("Input", curr);


        Mat input_gray;
        cv::Mat img_mask;
        cv::Mat img_bkgmodel;

        bool out_fct=false;

        //medianBlur(curr,curr,3);
        //medianBlur(curr,curr,3);

        cv::cvtColor(curr, curr, CV_BGR2GRAY);

        cv::equalizeHist(curr, curr);
        cv::GaussianBlur(curr, curr, cv::Size(7, 7), 1.5);
        if(prev.empty())
        {
          curr.copyTo(prev);

        }
        out_fct=frameDifferencing(curr,prev);
        if(out_fct)
        {
            cout<<"c'est du background :D"<<endl;
            chemin = dir_outputBG + images_path.at(i).fileName();
            imwrite(chemin.toStdString(), frame);


        }
        else

        { cout<<"existe véhicules"<<endl;
            chemin = dir_output_nonBG + images_path.at(i).fileName();

            imwrite(chemin.toStdString(), frame);


        }
    }

    return a.exec();
}



bool frameDifferencing( Mat img_input_prev, Mat img_input )
{

    int threshold=20;

    Mat img_foreground;

    if(img_input_prev.empty())
    {
      img_input.copyTo(img_input_prev);

    }

    cv::absdiff(img_input_prev, img_input, img_foreground);

    if(img_foreground.channels() == 3)
      cv::cvtColor(img_foreground, img_foreground, CV_BGR2GRAY);


     cv::threshold(img_foreground, img_foreground, threshold, 255, cv::THRESH_BINARY);

    // morphological operations
    int dilate_size =2;
    cv::Mat element_dilate = getStructuringElement( MORPH_RECT,
                                                Size( 2*dilate_size + 1, 2*dilate_size+1 ),
                                                Point( dilate_size, dilate_size ) );

    int erosion_size =1;
    cv::Mat element_erode = getStructuringElement( MORPH_RECT,
                                               Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                               Point( erosion_size, erosion_size ) );



    cv::erode(img_foreground,img_foreground,element_erode);
    cv::dilate(img_foreground,img_foreground,element_dilate);

    imshow("Foreground", img_foreground);

     cvWaitKey(1);
    if(!img_foreground.empty())
    {
        Mat nonZero;
        findNonZero(img_foreground, nonZero);

        double pixelWhite=(double)nonZero.rows/(img_foreground.rows*img_foreground.cols);

        cout << "pourcentage des blancs: "<<pixelWhite << endl;
        if(pixelWhite<0.001)
        {
            cout<<"c'est du background :D"<<endl;

            return true;
        }
        else


        { cout<<"existe véhicules"<<endl;


            return false;

        }


    }



}
